git clone https://olsosa08@bitbucket.org/olsosa08/week6ex10.git
mv week6Ex10.sh /home/$USER/week6ex10
cd /home/$USER/week6ex10
find . -name "*.pyc" -type f -delete
find . -name "__pycache__" -type d -delete
find . -name "*.class" -type f -delete 
find . -name "*.out" -type f -delete 
find . -name "*.txt" -type f -delete
git commit -a -m"Cleaned repo"
git add .
git commit -m"Added exercise script"
git push
